#!/bin/bash
cat abbrv.bib ref.bib > tmp.bib
biber --tool --configfile=biber.conf -output-file ref.bib tmp.bib
rm tmp.bib
